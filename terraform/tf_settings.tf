terraform {
  required_version = ">= 1.6.6"

  backend "http" {
    # authentication set locally
    address        = "https://gitlab.com/api/v4/projects/53433746/terraform/state/gitlab-runner-helm"
    lock_address   = "https://gitlab.com/api/v4/projects/53433746/terraform/state/gitlab-runner-helm/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/53433746/terraform/state/gitlab-runner-helm/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.12.1"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}