resource "helm_release" "gitlab-runner" {
  name       = "gitlab-runner-home"
  namespace  = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"

  values = [
    "${file("${path.module}/configs/values.yaml")}"
  ]
}